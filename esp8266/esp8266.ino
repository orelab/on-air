//
//  Board : NodeMCU 1.0 (ESP-12E Module)
//  Port : /dev/ttyUSB0
//
//  https://www.instructables.com/id/Quick-Start-to-Nodemcu-ESP8266-on-Arduino-IDE/
//

//-- EEPROM

#include <EEPROM.h>

//-- WIFI

#include <ESP8266WiFi.h>

const char *ssid = "SimplonLBGE";
const char *password = "SimplonLabege31*";

int ledPin = 13; // GPIO13 => D7
WiFiServer server(80);

//-- LED

#include <Adafruit_NeoPixel.h>

#define PIXEL_PIN 15 // GPIO15 => D8
#define PIXEL_COUNT 60

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);

int RED[3] = {255, 0, 0};       // Couleur Rouge
int GREEN[3] = {0, 255, 0};     // Couleur Verte
int CYAN[3] = {0, 255, 255};    // Couleur Cyan
int YELLOW[3] = {255, 125, 0};  // Couleur Jaune
int ORANGE[3] = {255, 40, 0};   // Couleur Orange
int PURPLE[3] = {255, 0, 255};  // Couleur Violette
int PINK[3] = {255, 0, 100};    // Couleur Rose
int BLUE[3] = {0, 0, 255};      // Couleur Bleu
int WHITE[3] = {255, 255, 255}; // Couleur Blanche
int OFF[3] = {0, 0, 0};         // Éteint

void setup()
{
  // LED DEMO

  strip.begin();
  strip.setBrightness(100);
  allLeds(RED);
  strip.show();

  Serial.begin(115200);

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  // Start the wifi
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(10);
  }
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/");
}

void loop()
{
  // Check wifi connection
  if(WiFi.status() != WL_CONNECTED)
  {
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED)
    {
      delay(10);
    }
  }


  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client)
  {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available())
  {
    delay(10);
  }

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();

  // Match the request

  if (request.indexOf("/LED=") == -1)
  {
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/html");
    client.println("");
    client.println("off.");

    return;
  }

  digitalWrite(ledPin, HIGH);

  if(request.indexOf("RED")!=-1) allLeds(RED);          // ok
  else if(request.indexOf("GREEN")!=-1) allLeds(GREEN); // ok
  else if(request.indexOf("CYAN")!=-1) allLeds(CYAN);   // ok
  else if(request.indexOf("YELLOW")!=-1) allLeds(YELLOW);
  else if(request.indexOf("ORANGE")!=-1) allLeds(ORANGE);
  else if(request.indexOf("PURPLE")!=-1) allLeds(PURPLE);
  else if(request.indexOf("PINK")!=-1) allLeds(PINK);
  else if(request.indexOf("BLUE")!=-1) allLeds(BLUE);   // ok
  else if(request.indexOf("WHITE")!=-1) allLeds(WHITE);
  else digitalWrite(ledPin, LOW);

  strip.show();


  // Return the response
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("");
  client.println("<!doctype html>");
  client.println("<html lang=\"fr\">");
  client.println("<head>");
  client.println("<title>ON AIR</title>");
  client.println("<meta charset=\"utf-8\">");
  client.println("<meta name=\"viewport\" content=\"width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no\">");
  client.println("</head><body>");
  client.println("<a href=\"/LED=RED\"\"><button>RED</button></a>");
  client.println("<a href=\"/LED=BLUE\"\"><button>BLUE</button></a>");
  client.println("<a href=\"/LED=GREEN\"\"><button>GREEN</button></a>");
  client.println("<a href=\"/LED=CYAN\"\"><button>CYAN</button></a>");
  client.println("<a href=\"/LED=OFF\"\"><button>Turn Off </button></a>");
  client.println("<p>" + String(millis()) + "</p>");
  client.println("</body></html>");
}

void allLeds(int COLOR[])
{
  for (int i = 0; i < PIXEL_COUNT; i++)
  {
    strip.setPixelColor(i, COLOR[0], COLOR[1], COLOR[2]);
  }
  strip.show();
}