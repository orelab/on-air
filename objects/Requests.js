const mongodb = require('mongodb');

exports.db = null;

exports.count = function()
{
    return new Promise((resolve, reject) => {
        this.db.find({}).toArray(function(err, result) {
            if(err) reject();
            resolve(result.length);
        });
    });
}

exports.get = function(query, users=null, onlytoday=true)
{
    if( onlytoday )
    {
        var d = new Date();
        d.setHours(0,0,0,0);

        query.timestamp = {$gt:d.getTime()};
    }

    return new Promise((resolveR,reject) => {
        this.db.find(query).toArray((err, result) => {
            if(err) {
                console.log(err);
                reject();
            }

            if( users )
            {
                let prom = result.map((item,i) => {

                    result[i].date = dateFormat( result[i].timestamp );
                    result[i].picto = ( result[i].msg=='pls' ) ? 'local_hospital' : 'help';
                    result[i].color = ( result[i].msg=='pls' ) ? 'red' : 'teal';
                    result[i].done = ( result[i].status != 'pending' );
    
                    return new Promise((resolveR2) => 
                        users.get({_id:mongodb.ObjectId(item.learner)}).then(u => {
                            result[i].user = u[0];
                            resolveR2();
                        })
                    );
                })

                Promise.all(prom).then(() => resolveR(result));
            }
            else
                resolveR(result);
        });
    });
}

exports.drop = function()
{
    this.db.drop();
}

exports.update = function(id, status, devices=null)
{
    return new Promise(resolve => 
        this.db.updateOne({_id:mongodb.ObjectId(id)}, {$set:{status:status}}, {}, e => {
            if(devices){
                devices.update(this);
            }
            resolve(e);
        })
    );
}

exports.save = function(msg, userid, status='pending', devices=null)
{
    return new Promise(resolve => 
        this.db.insertOne({
            timestamp: (new Date()).getTime(),
            msg: msg,
            learner: userid,
            status: status
        }, (err,res) => {
            if(devices){
                devices.update(this);
            }
            resolve(res.insertedId);
        })
    );
}

function dateFormat(timestamp)
{
    date = new Date(timestamp);

    return date.getDate() +'/'+ (date.getMonth()+1) +'/'+ date.getFullYear() +' '+
        date.getHours() +"h"+date.getMinutes();
}