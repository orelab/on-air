const bcrypt = require('bcrypt');
const data = require('./Database.js');

exports.db = null;

exports.generate = () =>
{
    data.team.forEach(e => 
    {
        bcrypt.hash(e.pass, 10, (err, hash)  =>
        {
            e.pass = hash;
            this.db.insertOne(e);
        });
    });
}

exports.count = function()
{
    return new Promise((resolve, reject) => {
        this.db.countDocuments({},{},function(err, nb) {
            if(err) reject();
            resolve(nb);
        });
    });
}

exports.drop = function()
{
    this.db.deleteMany({});
}

exports.show = function()
{
    this.db.find({}).toArray(function(err, result) {
    console.log(err ? err : result);
    });
}

exports.get = function(query)
{
    return new Promise((resolve,reject) => 
    {
        this.db.find(query).toArray((err, r) => 
        {
            if(err)
            {
                console.log(err);
                reject();
            }
            resolve(r);
        });
    });
}


exports.update = (query, update) =>
{
    return new Promise((resolve, reject) => 
    {
        this.db.updateOne(query, {$set:update})
            .then(r => resolve(r))
            .catch(err => reject(err));
    });
}