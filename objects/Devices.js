const http = require('http');
const axios = require('axios');
const data = require('./Database.js');

exports.db = null;

exports.generate = function()
{
    data.devices.forEach(e => this.db.insertOne(e));
}

exports.count = function()
{
    return new Promise((resolve, reject) => {
        this.db.countDocuments({},{},function(err, nb) {
            if(err) reject();
            resolve(nb);
        });
    });
}

exports.drop = function()
{
    this.db.deleteMany({});
}

exports.show = function()
{
    this.db.find({}).toArray(function(err, result) {
    console.log(err ? err : result);
    });
}

exports.get = function(query)
{
    return new Promise((resolve,reject) => {
        this.db.find(query).toArray((err, r) => {
            if(err) reject();
            resolve(r);
        });
    });
}


exports.update = function(requests)
{
    var d = new Date();
    d.setHours(0,0,0,0);

    let query = {
        status: 'pending',
        timestamp: {$gt:d.getTime()}
    };

    requests.get(query).then((result) => {
        console.log(`turning ${result.length?'on':'off'} all devices`);

        this.db.find({}).toArray(function(err, devices) {
            if( ! result.length )
                devices.forEach(e => axios.get(e.url + '/LED=OFF'));
            else
                devices.forEach(e => axios.get(e.url + '/LED=' + (result[0].msg=='pls'?'RED':'GREEN') ));
        });    
    });
}

