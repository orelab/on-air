
exports.team = [
    {status:"learner", name:"Mylène", pass:""},
    {status:"learner", name:"Marie", pass:""},
    {status:"learner", name:"Caroline", pass:""},
    {status:"learner", name:"Lucas", pass:""},
    {status:"learner", name:"Ridhoine", pass:""},
    {status:"learner", name:"Cécile", pass:""},
    {status:"learner", name:"Maxime", pass:""},
    {status:"learner", name:"Maréa", pass:""},
    {status:"learner", name:"Elodie", pass:""},
    {status:"learner", name:"Dimitri", pass:""},
    {status:"learner", name:"Cathy", pass:""},
    {status:"learner", name:"Emma", pass:""},
    {status:"learner", name:"Thomas", pass:""},
    {status:"learner", name:"Boris", pass:""},
    {status:"learner", name:"Abdellah", pass:""},
    
    {status:"teacher", name:"Allan", pass:""},
    {status:"teacher", name:"Aurélien", pass:""},
    {status:"teacher", name:"Nicolas", pass:""}    
];

exports.devices = [
    {name:"home", url:"http://dev.funworks.fr:666"},
    {name:"formation", url:"http://10.25.7.185"}
];
