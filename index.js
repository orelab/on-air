
//-- Requires

const mongodb = require('mongodb');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const session = require('express-session');
const mustacheExpress = require('mustache-express');
const bcrypt = require('bcrypt');

var http = require('http').Server(app);
var io = require('socket.io')(http);
app.io = io;

var users = require('./objects/Users.js');
var requests = require('./objects/Requests.js');
var devices = require('./objects/Devices.js');



//-- Configuration

require('dotenv').config();


//-- MongoDB

function dbConnect()
{
    return new Promise((resolve,reject) =>
    {
        let url = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/`;

        mongodb.MongoClient.connect(url, {useUnifiedTopology:true}, (err, db) =>
        {
            if (err) reject(err);
            let dbo = db.db(process.env.DB_NAME);

            users.db = dbo.collection('users');
            requests.db = dbo.collection('requests');
            devices.db = dbo.collection('devices');

            resolve(dbo);
        });
    });
}


//-- ExpressJS

function serverLaunch()
{
    app.engine('html', mustacheExpress());
    app.set('view engine', 'html');
    app.set('views', __dirname + '/views');
    
    app.use(express.static('public'));
    app.use(bodyParser.urlencoded({extended:false}));
    app.use(bodyParser.json());
    app.use(session(
    {
        name:'onair',
        secret: process.env.HTTP_SESSION,
        resave: false,
        saveUninitialized: false
    }));
    
    app.get('/', (req, res) => 
    {
        if( req.session.user )
        {
            var d = new Date();
            d.setHours(0,0,0,0);

            let query = 
            {
                learner: req.session.user._id,
                timestamp: {$gt: d.getTime()},
                status: 'pending'
            };

            requests.get(query,users).then(e => 
            {
                res.render('index', 
                {
                    login: req.session.user.name,
                    status: req.session.user.status,
                    pending: e.length ? e[0]._id : ''
                });
            });
        } else
            res.render('connection', 
            {
                pageid: 'connection',
                hideheader: true,
                hidemenu: true,
                hidefooter: true,
                nasaapi: process.env.NASA_API
            });
    });
    
    app.post('/', (req, res) => 
    {
        users.get({name:req.body.login}).then( u => 
        {
            if( u.length < 1 )
            {
                res.redirect('/');
                return;
            }

            bcrypt.compare(req.body.pass, u[0].pass, (err, valid) =>
            {
                if( valid )
                {
                    req.session.user = u[0];
                    req.app.io.emit('log', u[0].name+' is connected !');
                    res.redirect('/');
                }               
                else
                    res.render('connection');
            });
        });
    });
    
    app.get('/disconnect', (req, res) => 
    {
        delete req.session.user;
        res.redirect('/');
    });
    
    app.get('/admin', (req, res) => 
    {
        if( typeof req.session.user == 'undefined' )
        {
            //req.session.user={}; req.session.user._id = '5daa34dae383fc3d11de39a7';
            res.redirect('/');
            return;
        }

        let query = { _id: mongodb.ObjectId(req.session.user._id) };

        users.get(query).then( u =>
        {
            if(u[0].status == "teacher")
            {
                let p1 = requests.get( {status:'pending'}, users );
                let p2 = devices.get( {} );
                let p3 = requests.get( {}, users, false );
                let p4 = users.get( {} );

                Promise.all( [p1, p2, p3, p4] ).then( val => 
                    res.render('admin', {
                        status: req.session.user.status,
                        requests: val[0],
                        hasRequests: val[0].length,
                        devices: val[1],
                        hasDevices: val[1].length,
                        logs: val[2],
                        hasLogs: val[2].length,
                        users: val[3],
                        hasUsers: val[3].length,
                        title: 'Admin'
                    })
                );
            }
            else
                res.redirect('/');
        });
    });

    app.get('/account', (req, res) => 
    {
        if( typeof req.session.user == 'undefined' )
        {
            //req.session.user={}; req.session.user._id = '5daa34dae383fc3d11de39a7';
            res.redirect('/');
            return;
        }
        res.render('account', req.session.user);
    });

    app.post('/password', (req,res) => 
    {
        if( req.body.new1 != req.body.new2 )
        {
            res.send('Verification password do not match !');
            return;
        }

        if( ! bcrypt.compareSync(req.body.old, req.session.user.pass) )
        {
            res.send('Wrong old password !');
            return;
        }

        bcrypt.hash( req.body.new1, 10, (err, hash) =>
        {
            if( err )
            {
                res.send('Hash error');
                return;
            }

            let query = 
            {
                _id: mongodb.ObjectId(req.session.user._id)
            };

            let update = 
            {
                status: req.session.user.status,
                name: req.session.user.name,
                pass: hash
            };

            users.update(query, update).then(result =>
            {
                req.session.user.pass = hash;
                res.send('done.');

            }).catch(err => {
                console.log(err);
                res.send('error');
            });
        });
    });

    app.get('/help', (req, res) => 
    {
        requests.save('help', req.session.user._id, status='pending', devices).then( id =>
        {
            req.app.io.emit('notify', {
                level: 'help',
                msg: req.session.user.name+' need help !'
            });
            res.send(id);
        });
    });

    app.get('/pls', (req, res) => 
    {
        requests.save('pls', req.session.user._id, status='pending', devices).then( id =>
        {
            req.app.io.emit('notify', {
                level: 'pls',
                msg: req.session.user.name+' is PLS !'
            });
            res.send(id);
        });
    });

    app.get('/cancel/:id', (req, res) =>
        requests.update(req.params.id, 'canceled', devices).then(r =>
            res.send('done')
        )
    );

    app.get('/done/:id', (req,res) => 
    {
        requests.update(req.params.id, 'done', devices).then(r => {
            res.send('ok');
        });
    });

    /*
        These routes are for debug only
    */

    app.get('/resetall', (req,res) => 
    {
        requests.drop();
        users.drop();
        devices.drop();
        res.send('done.');
    });

    app.get('/forceupdate', (req, res) => 
    {
        devices.update(requests);
        res.send('done.');
    });

    return new Promise((resolve, reject) => 
    {
        app.on('error', () => reject() );
        http.listen(process.env.HTTP_PORT, () => resolve());
    });
}



//-- Program now !

dbConnect().then(
    () => serverLaunch().then(
        () => {
            console.log('HTTP server started on port ' + process.env.HTTP_PORT);

            users.count().then( c => 
            {
                if( c == 0 )
                {
                    users.generate();
                }
                //users.show();
            });

            devices.count().then( c => 
            {
                if( c == 0 )
                {
                    devices.generate();
                }
                //devices.show();
            });
        },
        () => console.log('HTTP server failed...')
    ),
    () => console.log('Database error...')
);





