# ON AIR
## An open platform to allow learners to ask for help in training sessions

This project is composed of a Node/Express application, which is connected to an IOT stuff, which is
lighted if a learner need help...


### The object

It is composed of a [3D printed box](https://www.thingiverse.com/thing:1506862), 
an ESP8266 prototype board and a piece of RGB LED strip.


### The app

It is build with Node/Express/Mustache/Notifications/Socket.io and MongoDB. The database works with 3 following collections :

#### Users

It is simply the list of users : _id,status,name,pass. The status can be 'learner' or 'teacher'. In fact, it deals with rights. A teacher can access the 'admin' page and declare a request as 'done'.

#### Requests

When a learner needs help, a request is created. The request can have many statuses :
- pending : waiting for a teacher help
- done : the teacher made the job
- canceled : the learner don't need anymore to be helped, before a teacher come

#### IOT

This is the list of connected object that can be called by the app. Each time a request is created,
or eac h time an existing request status is changed, the app look if the IOT must be set to 'on' or 'off'.

(note : work in progress...)