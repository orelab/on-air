
/*
    Change password
*/
var pw = document.getElementById('changepw');

if( pw )
{
    pw.addEventListener('click', () =>
        axios.post('/password',
            {
                old: document.getElementById('old').value,
                new1: document.getElementById('new1').value,
                new2: document.getElementById('new2').value
            })
            .then(response => alert(response.data) )
    );
}


/*
    HELP button
*/
var help = document.getElementById('help');

if( help )
{
    help.addEventListener('click', function(){
        axios.get('/help')
        .then(function (response) {
            document.getElementById('action-none').style.display = 'none';
            document.getElementById('action-pending').style.display = 'block';
            document.getElementById('cancel').setAttribute('data-id', response.data);
        });
    });
}


/*
    PLS button
*/
var pls = document.getElementById('pls');

if( pls )
{
    pls.addEventListener('click', function(){
        axios.get('/pls')
        .then(function (response) {
            document.getElementById('action-none').style.display = 'none';
            document.getElementById('action-pending').style.display = 'block';
            document.getElementById('cancel').setAttribute('data-id', response.data);
        });
    });
}


/*
    PLS button
*/
var cancel = document.getElementById('cancel');

if( cancel )
{
    cancel.addEventListener('click', function(){

        let id = this.getAttribute('data-id');

        axios.get(`/cancel/${id}`)
        .then(function (response) {
            document.getElementById('action-none').style.display = 'block';
            document.getElementById('action-pending').style.display = 'none';
        });
    });

    /*
        During interface loading, if the connected user has ever made a request,
        the interface is switched in 'cancel' mode.
    */
    if( cancel.getAttribute('data-id') ){
        document.getElementById('action-none').style.display = 'none';
        document.getElementById('action-pending').style.display = 'block';
    }
}


/*
    Turn off buttons
*/
Array.from(document.getElementsByClassName('turnoff')).forEach(e => {
    e.addEventListener('click', () => {
        axios(`/done/${e.id}`).then(f => {
            if( f.data == 'ok' ){
                e.parentNode.parentNode.remove();
            }
        });
    });
});


/*
    Materialize tabs
*/
M.Tabs.init(
    document.querySelectorAll('.tabs'),
    {swipeable:false}
);


/*
    Socket.io
*/

var socket = io();

var status = document.querySelector("body").getAttribute('data-status');


if( status == "teacher" )
{
    socket.on('log', msg => console.log(msg));
    socket.on('notify', msg => new Notification("ON AIR", {body: msg.msg}));
}



/*
    Notifications
*/

window.addEventListener('load', function ()
{
    Notification.requestPermission(function (status)
    {
        // Cela permet d'utiliser Notification.permission avec Chrome/Safari
        if (Notification.permission !== status) 
        {
            Notification.permission = status;
        }
    });
});



